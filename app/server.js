require('./db/connection');
const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const https = require('https');
const cheerio = require('cheerio');
const app = express();
const PORT = 3000;

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded());

const url = 'https://fr.uefa.com/uefachampionsleague/season=2019/clubs/';

function getFile() {
    https.get(url, (res) => {
        res.pipe(fs.createWriteStream('./uefa.html')); 
        res.on('end', () => {
            console.log('Fichier enregistré')
        })
    })
}
// getFile();

 function parseFile(path) {
    fs.readFile(path, (err, file) => {
        if (err) console.log(err);
        
        const $ = cheerio.load(file.toString());
        var teams = $('div.team team-is-club');
    })
}

parseFile('./uefa.html');

app.listen(PORT, () => console.log(`Server connected on port ${PORT}...`))